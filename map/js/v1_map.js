"use strict";

var positron = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png', {});


var map1 = L.map('map1', {
    center: [19.711510, -4.935242],
    zoom: 2,
    layers: [positron],
});

var baseLayers = {
    "Grayscale (CartoDB)": positron,
};

var d3Layer = L.Class.extend({
    initialize: function() {
        return;
    },
    onAdd: function() {
        d3.select("div#map1 .legend").style("display", "block");
        d3.select("div#map1 .regions").style("display", "block");
    },
    onRemove: function() {
        d3.select("div#map1 .regions").style("display", "none");
        d3.select("div#map1 .legend").style("display", "none");
    },
});

var svgLayer = new d3Layer();

var overlays = {
    "GeoJSON Regions": svgLayer
};
L.control.layers(baseLayers, overlays).addTo(map1);

var svgMap = d3.select(map1.getPanes().overlayPane).append("svg"),
    g = svgMap.append("g").attr("class", "leaflet-zoom-hide regions"),
    g_circles = svgMap.append("g").attr("class", "leaflet-zoom-hide");

// Define the div for the tooltip
var div = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

var color = d3.scale.linear()
    .range(colorbrewer.Blues[3])

var showValue = 'DDD1000';
var record = [];

function addRecord(d) {

    d[showValue] = +d[showValue];
    var obj = {
        key: d.Country,
        value: d[showValue]
    };
    record.push(obj);
    return d;
}

function render_map(collection) {

    var transform = d3.geo.transform({
            point: projectPoint
        }),
        path = d3.geo.path().projection(transform);

    function reporter(x) {

        var item = {
            country: "None",
            value: "None"
        }

        record.forEach(function(d) {
            if (x.properties.ADMIN == d.key) {
                item.country = d.key;
                item.value = d.value;
                return;
            }
        });
        return item;
    }

    function getColor(x) {

        var value = -1;

        record.forEach(function(d) {
            if (x.properties.ADMIN == d.key) {
                value = d.value;
                return;
            }
        });
        if (value == -1) {
            return "none";
        }
        return color(value);
    }

    var feature = g.selectAll("path")
        .data(collection.features)
        .enter().append("path")
        .attr("fill", getColor)
        .attr("stroke", "grey")
        .attr("stroke-width", 1)

    feature.attr("d", path)
        .on("mouseover", function(d) {
            var item = reporter(d);
            if (item.country != "None") {
                d3.select(this)
                    .transition()
                    .duration(200)
                    .style("fill", "#DB7093");
                div.transition()
                    .duration(200)
                    .style("opacity", .9);
                div.html(
                        "<span><b>" + item.country + "</b></span>" +
                        "<br></br>" +
                        "<span>" + item.value + " Standard Units per 1000 Pop </span>")
                    .style("left", (d3.event.pageX + 10) + "px")
                    .style("top", (d3.event.pageY + 10) + "px")
            }
        })
        .on("mouseout", function(d) {
            d3.select(this)
                .transition()
                .duration(200)
                .style("fill", getColor);
            div.transition()
                .duration(200)
                .style("opacity", 0);
        });

    // Handle zoom of the map and repositioning of d3 overlay
    map1.on("viewreset", reset);
    reset();

    function reset() {

        var bounds =
            path.bounds(collection),
            topLeft = bounds[0],
            bottomRight = bounds[1];
        svgMap.attr("width", bottomRight[0] - topLeft[0])
            .attr("height", bottomRight[1] - topLeft[1])
            .style("left", topLeft[0] + "px")
            .style("top", topLeft[1] + "px");
        g.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");
        g_circles.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");

        feature.attr("d", path)
            .attr("stroke", "grey")
            .attr("stroke-width", 1)
            .attr("fill", getColor)

    }

    // Use Leaflet to implement a D3 geometric transformation.
    function projectPoint(x, y) {
        var point = map1.latLngToLayerPoint(new L.LatLng(y, x));
        this.stream.point(point.x, point.y);
    }

}

d3.csv("data/heatmap.csv", addRecord, function(data) {
    color.domain(d3.extent(data, function(d) {
        return d[showValue];
    }));

    d3.json("data/countries.geojson", render_map);


});
