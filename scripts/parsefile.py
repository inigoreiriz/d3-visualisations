import pandas as pd
import json

def parse_csv():

    df = pd.read_excel('../map/data/antibiotic.xlsx')
    df = df[df.Year == 2014]
    grouped = df.groupby('Country')['DDD/1000 inh/day'].sum()

    dt = pd.DataFrame()
    dt['Country'] = grouped.index
    dt['DDD1000'] = grouped.values

    dt.to_csv('../map/data/heatmap.csv', index=False)

    return dt

def parse_geojson():

    dt = parse_csv()

    with open('../map/data/countries.geojson') as f:
        geojson = json.load(f)

    match = []
    for item in range(len(geojson['features'])):
        geo = geojson['features'][item]['properties']['ADMIN']
        if geo in dt.Country.values:
            match.append(geojson['features'][item])

    new_geojson = {}
    new_geojson['type'] = "FeatureCollection"
    new_geojson['features'] = match

    with open('../map/data/new.countries.geojson', 'w') as f:
        json.dump(new_geojson, f)

def main():

    parse_geojson()

if __name__ == '__main__':
    main()
