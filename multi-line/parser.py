import pandas as pd
import glob

for filename in glob.glob('data/*'):
    data_xls = pd.read_excel(filename)
    data_xls.to_csv('data/file.csv', encoding='utf-8', index=False)
