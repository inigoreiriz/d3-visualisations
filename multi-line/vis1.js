
     var outerWidth = 500;
     var outerHeight = 250;
     var margin = { left: 70, top: 5, right: 5, bottom: 60 };
     var xColumn = "year";
     var yColumn = "value";
     var xAxisLabelText = "Year";
     var xAxisLabelOffset = 48;
     var yAxisLabelText = "Value";
     var yAxisLabelOffset = 40;
     var innerWidth  = outerWidth  - margin.left - margin.right;
     var innerHeight = outerHeight - margin.top  - margin.bottom;

     // Define the div for the tooltip
     var div = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

     var svg = d3.select("body").append("svg")
       .attr("width", outerWidth)
       .attr("height", outerHeight);

     var g = svg.append("g")
       .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

     var xAxisG = g.append("g")
       .attr("class", "x axis")
       .attr("transform", "translate(0," + innerHeight + ")")

     var xAxisLabel = xAxisG.append("text")
       .style("text-anchor", "middle")
       .attr("transform", "translate(" + (innerWidth / 2) + "," + xAxisLabelOffset + ")")
       .attr("class", "label")
       .text(xAxisLabelText);

     var yAxisG = g.append("g")
       .attr("class", "y axis");

     var yAxisLabel = yAxisG.append("text")
       .style("text-anchor", "middle")
       .attr("transform", "translate(-" + yAxisLabelOffset + "," + (innerHeight / 2) + ") rotate(-90)")
       .attr("class", "label")
       .text(yAxisLabelText);

     var xScale = d3.time.scale().range([0, innerWidth]);
     var yScale = d3.scale.linear().range([innerHeight, 0]);

     var xAxis = d3.svg.axis().scale(xScale).orient("bottom")
       .ticks(5)
       .tickFormat(d3.format("s"))
       .outerTickSize(2);

     var yAxis = d3.svg.axis().scale(yScale).orient("left")
       .ticks(5)
       .tickFormat(d3.format("s"))
       .outerTickSize(2);

     var lineFunction = d3.svg.line()
       .x(function(d) { return xScale(d[xColumn]); })
       .y(function(d) { return yScale(d[yColumn]); })
       .interpolate("linear");

     function render(data){
       xScale.domain(d3.extent(data, function (d){ return d[xColumn]; }));
       yScale.domain(d3.extent(data, function (d){ return d[yColumn]; }));
       xAxisG.call(xAxis);
       yAxisG.call(yAxis);

       g.append("path")
          .attr("class", "chart-line")
          .attr("d", lineFunction(data));

       g.selectAll("dot")
           .data(data)
       .enter().append("circle")
           .attr("r", 5)
           .attr("cx", function(d) { return xScale(d[xColumn]); })
           .attr("cy", function(d) { return yScale(d[yColumn]); })
           .on("mouseover", function(d) {
               div.transition()
                   .duration(200)
                   .style("opacity", .9);
               div	.html(d[xColumn])
                   .style("left", (d3.event.pageX) + "px")
                   .style("top", (d3.event.pageY - 28) + "px");
               })
           .on("mouseout", function(d) {
               div.transition()
                   .duration(500)
                   .style("opacity", 0);

        });
     }

     function type(d){
       d.year = +d.year;
       d.value = +d.value;
       return d;
     }

     d3.csv("data/file.csv", type, render);
